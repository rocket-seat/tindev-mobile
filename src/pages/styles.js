// import styled from 'styled-components/native';

// export const Container = styled.View`
//     flex: 1;
//     background-color: '#f5f5f5';
//     justify-content: center;
//     align-items: center;
//     padding: 30px;
// `;

// export const Button = styled.TouchableOpacity`
//     height: 46px;
//     background-color: '#df4723';
//     align-self: 'stretch';
//     border-radius: 4px;
//     margin-top: 10px;
//     justify-content: center;
//     align-items: center;
// `;

// export const ButtonText = styled.Text`
//     color: '#fff';
//     font-size: 16px;
//     font-weight: bold;
// `;

// export const Input = styled.TextInput`
//     height: 46px;
//     background-color: '#fff';
//     align-self: 'stretch';
//     border-width: 1;
//     border-color: '#ddd';
//     border-radius: 4px;
//     margin-top: 20px;
//     padding: 0 15px;
// `;

import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f5f5f5',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 30,
    },
    input: {
        height: 46,
        backgroundColor: '#fff',
        alignSelf: 'stretch',
        borderWidth: 1,
        borderColor: '#ddd',
        borderRadius: 4,
        marginTop: 20,
        paddingHorizontal: 15,
    },
    button: {
        height: 46,
        backgroundColor: '#df4723',
        alignSelf: 'stretch',
        borderRadius: 4,
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 16,
    }
})

export default styles