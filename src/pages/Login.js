import React, { useState, useEffect } from 'react'
import { KeyboardAvoidingView, Platform, Text, Image, TextInput, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import api from '../services/api';

import styles from './styles';
import logo from '../assets/logo.png';

const Login = ({ navigation }) => {

    const [user, setUser] = useState('')

    useEffect(() => {
        AsyncStorage.getItem("user").then(user => {
            if (user) {
                navigation.navigate('Main', { user })
            }
        })

    }, [])

    async function handleLogin() {
        const res = await api.post('/devs', { username: user })
        const { _id } = res.data;

        await AsyncStorage.setItem("user", _id)

        navigation.navigate('Main', { user: _id })
    }

    return (
        <KeyboardAvoidingView style={styles.container} behavior="padding" enabled={Platform.OS === 'ios'}>
            <Image source={logo} />

            <TextInput autoCapitalize="none" autoCorrect={false} placeholderTextColor="#999" placeholder="Digite seu usuário do Github" style={styles.input} value={user} onChangeText={setUser} />

            <TouchableOpacity style={styles.button} onPress={handleLogin}>
                <Text style={styles.buttonText}>Enviar</Text>
            </TouchableOpacity>
        </KeyboardAvoidingView>
    )
}

export default Login